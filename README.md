# BrickBreaker

![logo](/pictures/logo.png)


This is a small project to get to know the `pygame` library. The game is a simple adoption of the well-known game [Brick Breaker](https://en.wikipedia.org/wiki/Brick_Breaker).

The player can horizontally move a platform, to control a ball to smash several layers of bricks. After smashing a brick, a small chance exists that a power-up appears. Power-ups can either increase or decrease the speed the platform moves or generate a bomb that, if collected by the player, instantly terminates the game.


The impelentation is based on the pygame tutorial [Discover Python and Patterns](https://www.patternsgameprog.com/series/discover-python-and-patterns/).


## Impression

![screenshot](/pictures/brick_breaker.png)

## Setup

For development:

```bash
pip install -e .
```

Run:

`python run.py`

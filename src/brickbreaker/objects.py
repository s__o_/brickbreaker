from abc import abstractmethod, ABC

import random
import pygame


SPEED = 0
SLOW = 20
GHOST = 60


class Object(ABC):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.texture = pygame.image.load("../assets/units.png")

    @abstractmethod
    def render(self, surface):
        pass


class Platform(Object):
    def __init__(self, x, y):
        super(Platform, self).__init__(x, y)

        self.speed = 20
        self.width = 120
        self.height = 20

        self.dir_buffer = []

    def move(self, surface, dir):
        if len(self.dir_buffer) > 10:
            self.dir_buffer.pop(0)

        self.dir_buffer.append(dir)

        self.x += dir * self.speed

        # restrict to window boundaries
        if self.x <= 0:
            self.x = 0
        elif self.x >= surface.get_width() - self.width:
            self.x = surface.get_width() - self.width

    def render(self, surface):
        rect = pygame.Rect(20, 0, self.width, self.height)
        location = pygame.math.Vector2(self.x, self.y)
        surface.blit(self.texture, location, rect)


class Ball(Object):
    def __init__(self, x, y):
        super(Ball, self).__init__(x, y)

        self.speed = 4
        self.size = 20
        self.spin = 1
        self.v_dir = 1
        self.h_dir = 1

    def move(self, surface, platform: Platform):
        new_y = self.y + (self.v_dir * self.speed)

        if (
            new_y >= platform.y - 10
            and self.x >= platform.x
            and self.x <= platform.x + platform.width
        ):
            # collision with platform
            self.v_dir = -1
            new_y = self.y + (self.v_dir * self.speed)
            if sum(platform.dir_buffer) > 0:
                self.h_dir = 1
            elif sum(platform.dir_buffer) < 0:
                self.h_dir = -1
        elif new_y <= 0:
            self.v_dir = 1
            new_y = self.y + (self.v_dir * self.speed)

        new_x = self.x + (self.h_dir * self.spin)

        if new_x >= surface.get_width() or new_x <= 0:
            self.h_dir = self.h_dir * -1
            new_x = self.x + (self.h_dir * self.spin)

        self.y = new_y
        self.x = new_x

    def render(self, surface):
        rect = pygame.Rect(0, 0, self.size, self.size)
        location = pygame.math.Vector2(self.x, self.y)

        surface.blit(self.texture, location, rect)


class PowerUp(Object):
    def __init__(self, x, y):
        super(PowerUp, self).__init__(x, y)

        self.speed = 2
        self.size = 20
        self.spin = 1
        self.v_dir = 1
        self.h_dir = 0
        self.type = None
        self.activated = False

    def move(self, surface, ball, platform: Platform):
        self.y = self.y + self.speed

        if (
            self.y >= platform.y - 10
            and self.x >= platform.x
            and self.x <= platform.x + platform.width
        ):
            # collision with platform
            if self.activated is False:
                self.activate(ball, platform)
                self.y = 1000
                self.activated = True

    def render(self, surface):
        rect = pygame.Rect(self.type, 20, self.size, self.size)
        location = pygame.math.Vector2(self.x, self.y)

        surface.blit(self.texture, location, rect)

    @abstractmethod
    def activate(self, ball, platform):
        pass


class PowerUpSpeed(PowerUp):
    def __init__(self, x, y):
        super(PowerUpSpeed, self).__init__(x, y)

        self.type = SPEED

    def activate(self, ball, platform):
        platform.speed += 4

        if platform.speed > 30:
            platform.speed = 30


class PowerUpSlow(PowerUp):
    def __init__(self, x, y):
        super(PowerUpSlow, self).__init__(x, y)

        self.type = SLOW

    def activate(self, ball, platform):
        platform.speed -= 4

        if platform.speed < 10:
            platform.speed = 10


class PowerUpGhost(PowerUp):
    def __init__(self, x, y):
        super(PowerUpGhost, self).__init__(x, y)

        self.speed = 0.5
        self.type = GHOST

    def activate(self, ball, platform):
        ball.y = platform.y + 20


class Block(Object):
    def __init__(self, x, y):
        super(Block, self).__init__(x, y)

        self.width = 120
        self.height = 30

        colors = [0, 120, 240, 360]

        self.color = colors[random.randint(0, len(colors) - 1)]

    def render(self, surface):
        rect = pygame.Rect(self.color, 40, self.width, self.height)
        location = pygame.math.Vector2(self.x, self.y)

        surface.blit(self.texture, location, rect)

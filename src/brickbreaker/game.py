import pygame
import random

from brickbreaker.objects import (
    Platform,
    Ball,
    Block,
    PowerUpSpeed,
    PowerUpSlow,
    PowerUpGhost,
)


class UserInterface:
    def __init__(self):
        pygame.init()
        self.window = pygame.display.set_mode((1200, 480))
        pygame.display.set_caption("BrickBreaker")
        pygame.display.set_icon(pygame.image.load("../pictures/logo.png"))
        self.clock = pygame.time.Clock()
        self.POWER_UP_RATE = 0.4
        self.moveCommandX = 0
        self.moveCommandY = 0
        self.running = True

        self.platform = Platform(120, 400)
        self.ball = Ball(120, 200)

        self.blocks = []

        for row in range(0, 4):
            for col in range(0, int(self.window.get_width() / 120)):
                self.blocks.append(Block(col * 120, row * 30))

        self.obj = []

    def processInput(self):
        self.moveCommandX = 0
        self.moveCommandY = 0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
                break
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
                    break
                elif event.key == pygame.K_RIGHT:
                    self.moveCommandX = 1
                elif event.key == pygame.K_LEFT:
                    self.moveCommandX = -1

    def update(self):
        self.platform.move(self.window, self.moveCommandX)
        self.ball.move(self.window, self.platform)

        i = None
        for idx, block in enumerate(self.blocks):
            if self.ball.y <= block.y + block.height and (
                self.ball.x >= block.x
                and self.ball.x <= (block.x + block.width)
            ):
                block = self.blocks.pop(idx)
                self.ball.v_dir = self.ball.v_dir * (-1)
                self.ball.move(self.window, self.platform)

                if random.random() > self.POWER_UP_RATE:
                    i = random.randint(0, 2)
                    if i == 0:
                        self.obj.append(
                            PowerUpSpeed(block.x + (block.width / 2), block.y)
                        )
                    elif i == 1:
                        self.obj.append(
                            PowerUpSlow(block.x + (block.width / 2), block.y)
                        )
                    elif i == 2:
                        self.obj.append(
                            PowerUpGhost(block.x + (block.width / 2), block.y)
                        )

                break

        remove = []
        for idx, o in enumerate(self.obj):
            o.move(self.window, self.ball, self.platform)
            if o.y > self.platform.y:
                remove.append(idx)

        for item in remove:
            self.obj.pop(item)

        if self.ball.y > self.platform.y or len(self.blocks) == 0:
            self.running = False

    def render(self):
        self.window.fill((12, 154, 219))

        self.platform.render(self.window)
        self.ball.render(self.window)

        for b in self.blocks:
            b.render(self.window)

        for o in self.obj:
            o.render(self.window)

        pygame.display.update()

    def run(self):
        while self.running:
            self.processInput()
            self.update()
            self.render()
            self.clock.tick(60)

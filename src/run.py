import pygame
from brickbreaker.game import UserInterface

def start_game():
    userInterface = UserInterface()
    userInterface.run()

    pygame.quit()


if __name__ == "__main__":
    start_game()
